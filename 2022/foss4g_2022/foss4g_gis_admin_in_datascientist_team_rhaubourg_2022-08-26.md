# notes perso FOSS4G


# QGIS hackfest

# Networking

Tobias ZEA renewable 

## hack

- fix automatic translation update po files 
- improve documentation landing page for plugin building
    - add links to minimal plugin template and qgis plugin ci plugin / cookie cutter


## Strategy discussion


- Funding QGIS : https://demo.hedgedoc.org/-t2TwUOcTE6YrwZpL45KaA# 
- The Vision for QGIS : Discussion to clarify our raison d'être. Très inspiré par Blender
- candidate to the PSC ? 
- Apply to NumFocus

## Workshop MapLibre + React for digital twins

Ugly workshop. Unprepared. JS folks just mess computers 

Sebastian Lopez (ARG) + Walter Shilma 

KAN-T-IT : small company - digital twins

https://github.com/Kan-T-IT/FOSS4G2022-Workshop-Maplibre-React

https://github.com/sebastianoscarlopez/foss4g2022-react-maplibre

interesting stuffs
ifc tools fo js : https://ifcjs.github.io/info/

## PyGeoAPI 

top préparation https://dive.pygeoapi.io/setup/ 

Enseignements 

- un simple yml pour la configuration
- swagger landing page http://localhost:5000/openapi 
- ogc landing page http unique pour tous les services://localhost:5000/
- skipGeometry flg to gest features without geometries
- un bug QGIS sur la pagination https://github.com/qgis/QGIS/issues/49404

en conclusion, très chaud pour déployer ça, peu trop de cout de maintenance et de configuration par rapport aux autres configurations et ça va dans le bon sens de la standardisation. Il va nous falloir en point d'entrée dans bdnb.eu 


## collaboration OSM - US govt

Maggie Cawley


Mêmes discussions qu'avec la fabrique des géocommuns

sujet concret

Post covid engouement pour la rando. Des traces OSM partout dans les parcs nationaux. dans des zones dangereuses, protégées, beaucop de secours
instagram danger
 - groupe de travail OSM US dont 50% osm contrib
 - go fixing data IN OSM, after testing investigation, education campaign to osm contributors etc..

OSM licence incompatibiliy 
one way only 


## Geopandas nouveauté

gdf.explore() > folium interactive map

to_postgis() improvements

improving bindings performance

Shapely v2 > array based / fast
. No more need to do manual "for loop"

lecture écriture via gdal ogr  > 5-20 x plus rapide
 

Support GeoParquet, 2-3x plus rapide

Accès monocolonne ultra efficient

compressé


introducing dask-geopandas

```: chunks dataframe > task graphs > parallel computing


conda install pyogrio
```

Utilisation du format apache ARROW comme interface avec GDAL !!  > Encore 2x plus rapide (pas encore released)


## Maplibre

Typescript rewrite 

terrain support

no more mapbox specifics

maxPitch 

support pointcloud

intégration angular

## Mapstore

interface web dédié dashboard charts et stories 
mobile first

a product

vraiment intéressant pour valoriser nos contenus publiquement

page d'accueil 

## point cloud

CPOC

QGIS depuis la 3.26 utilise copc en interne 

optimisé web access : on dépose sur une url http, comme le coggeotif

pdal + gdal supportent ça

api pour python pour explorer dans des notebook

entwine est une alternative pour exposer sous le même format octree un regroupement de fichiers laz. là ou copc stocke les données dans un seul laz au format octree (peut éviter de faire un huge laz / analogie avec vrt gdal)



## segmentation de points clouds

semantic segmentation : labeling point clouds

KPConv

## Building 3D object from pointclouds 

Oslandia : geo3dfeatures 

IASBIM ?


### spatio temporal pg 

timescale db 
H3 index


## financement potentiel

Hannes Reuter, https://talks.osgeo.org/foss4g-2022/talk/QDYYBB/ 
GISCO team (GIS COordination) 

financement direct potentiel via deux types de fonds

- fonds statistiques 

les fonds sont versés aux INSEE des états membres

.. qui peuvent éventuellement sous traiter


Financement annuel. Cette année, échéance ~ 25 septembre

Pour que ça passe, ça doit être badgé comme une étude stat au minimum. Comme par exemple améliorer l'automatisation des études stats, sortir des nouveaux indicateurs à l'échelle IRISetc..

Idée : financer la fabrique des géocommuns en l


## divers

- explorer AppImage pour builder QGIS linux à la place de docker ? 


