---
title: FOSS4G 2022 - BNDB, a national building's open database to ease massive retrofitting policies - Régis Haubourg
tags: presentation,GIS,opendata,FOSS4G
type: slide
description: talk given at FOSS4G 2022 Firenze. this document is released under CC-BY-SA licence
lang: en-US
breaks: true
slideOptions:
  transition: fade
  theme: moon
  
---

## BDNB

### the first french open database of buildings

_Built for open data and with FOSS4G_




<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---


<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/809698a5-4c33-4ffa-a7f9-aa499ff86b0d.png" data-background-opacity="1" -->

---


<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/db881716-3761-4079-b12f-4c28d354a73b.png" data-background-opacity="1" -->

---

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/8c510269-6c16-4458-bafa-785330a40b84.png" data-background-opacity="1" -->


```
Régis Haubourg

--
    
GIS and data administrator 
Research engineer at CSTB 

OSGEO charter member since 2016. 
OSGeo-FR local Chapter president 2019-2021

QGIS funder  &  contributor

```

---

![CSTB image](https://codimd.s3.shivering-isles.com/demo/uploads/ff82fbeb-319a-4256-b999-8d9777bcb400.png)

### [CSTB](http://www.cstb.fr/en/cstb/missions-activities/) : French Scientific and Technical Center for Building


- Assess construction norms standards
- Monitor and builds models (energy, comfort, interior pollutants, sound, fire ,  wind, etc..)
- Global Life Cycle Assessment (carbon, pollutants, water, biodiversity, ..) 



---

![CSTB image](https://codimd.s3.shivering-isles.com/demo/uploads/ff82fbeb-319a-4256-b999-8d9777bcb400.png)

### Testing and certification :

Risks, rain, snow, aucoustic behavior, magnetic fields..

![](https://codimd.s3.shivering-isles.com/demo/uploads/4321883a-661e-45b5-9300-f1adec617e20.png)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/3d4e99e1-ac6b-4502-a588-f05a150f34f0.png)

![](https://codimd.s3.shivering-isles.com/demo/uploads/425e303e-8ddd-4d6d-93cb-69cf01c638c1.png)

---

### CSTB as a service provider

- Equipments and material testing, evaluation and certification
- Public policy assessment
- 3D / BIM

---

### BIM 


![EveBIM](https://codimd.s3.shivering-isles.com/demo/uploads/0e04493e-d28b-4331-850a-f674565640f4.png)


---

### and now .. digital strategies

GIS, databases

using FOSS4G and opendata

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---

## Rationales of a reference dataset of buildings ?

---

- *1/3* of Carbon emissions are linked to building and housing
- 50% to 70% of them are emitted during the build-up process !
- Massive retrofitting is the only path to keep on Paris Agreement trajectory
- Heat waves.. :fire: :fire: .. you know.. 


---

#### We need data to assess *all** policies



---

### Crossing multiple datasources to build the best possible database (BDNB)

with all possible relevant informations

---

![overview_bdnb_construction](https://codimd.s3.shivering-isles.com/demo/uploads/b673d5a9-e5e6-4dec-96ce-a70f79e54ab8.png)

---

### And build web applications on top of it

---

#### For public audience 

Encourage retrofitting decisions to retrofit decision


![G_to_A](https://codimd.s3.shivering-isles.com/demo/uploads/c3056cf7-96f8-43fb-b3b9-3c261c032da1.jpeg)



---

![particulier1](https://codimd.s3.shivering-isles.com/demo/uploads/603fe79a-d6db-4420-9cf4-25731eaedc37.png)

---

![particulier2](https://codimd.s3.shivering-isles.com/demo/uploads/da274b4e-bfe0-4c28-9a6c-b757b9259fd3.png)

---

![particulier_mobile](https://codimd.s3.shivering-isles.com/demo/uploads/86a3670b-9c2a-4f39-8b14-4edb49042347.png)

---

#### For more advanced usage

A generic GIS app with Maplibre, Angular, PostgREST and PG



---

![lab 3 tours grenoble DPE predict](https://codimd.s3.shivering-isles.com/demo/uploads/99c74fbd-2473-418b-ad10-443f0b6c2ce6.png)

---

![grenoble lab large scale](https://codimd.s3.shivering-isles.com/demo/uploads/f58a5d6c-16f1-493b-8e44-b5b04001b390.png)

---

![One building lab ](https://codimd.s3.shivering-isles.com/demo/uploads/729b9b65-5b81-4624-91f7-c80d679cce84.png)

---

#### A set of advanced services

For expert and datascientists

- REST API (OGCAPIF coming!)
- authentication
- Python notebooks for jupyterlab
- SQL access 
- QGIS GIS environnement


---

### For social housing landlords


---

![](https://codimd.s3.shivering-isles.com/demo/uploads/8b14ba4a-e96c-4272-b299-12aa510f2db7.png)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/de77bdf2-56c5-4677-b0ab-1121c25cf327.png)


---

![](https://codimd.s3.shivering-isles.com/demo/uploads/67073496-9805-4b46-80c1-5948d342eb5e.jpg)

#### Funded thanks to GORENOVE project 

2019-2021

Funds by Carbon and energy saving credits (Carbon permits and trading market)

Lead by ProFEEL = CSTB + Building professionals "obliged" by carbon market. 

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/8dfec58a-1c7d-4269-8eec-b0519e3d1576.png)


---

#### Possible use cases 

- How to speed up Life cycle Analysis on a city/quarter scale ?
- What buildings to retrofit first ? Where ?
- How to improve city to cool them down ?
- Provide more accurate estimates at country wide scale on any topic
- Help stakeholders to build information system WITH building information

---

Latest real question

#### _"We will miss natural gaz next winter. What buildings are the biggest consumers? "_

![](https://codimd.s3.shivering-isles.com/demo/uploads/b649f58a-caeb-4cae-b862-41234698248c.png =500x)

-> Learnings, Swimming pools, stadiums, and ice skating rinks 

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/8bb549d3-0291-470c-9968-df110f20acf3.jpg)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/bbc14e08-4c32-4e22-ae37-6741964ca7cd.jpg)


---

## Challenges 

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---

### But what is a Building ?

- No common definition 
- No centralized information system

![real cases of buildings](https://codimd.s3.shivering-isles.com/demo/uploads/34d682e1-ff69-4f2f-a4ba-dd56aee4e618.png)


---

#### Core data sources

##### Tax ministry "Fichiers fonciers"

- **Partially** open data
- Geometry footprints not linked to attribute data
- geometry is not accurate
- but VERY rich attribute wise

-> we obtained a clearance to use them 
 

---



##### French Geographic Institute( IGN) povides building footprints

- nice geometry footprints 
- very few attributes
- opendata only recently
- consistent with aerial imagery +/-50cm
 
![](https://codimd.s3.shivering-isles.com/demo/uploads/7eef84d7-2013-4024-bf75-8f9f0c5239cd.png)


---


### Street address are de-facto identifiers

- 3 concurrent Street address databases until 2015

- Now we have the Base Adresse Nationale (BAN)

![](https://codimd.s3.shivering-isles.com/demo/uploads/119a418c-1349-49e7-80ff-32c9645db536.png)

- Most derived dataset with poor adress quality

---

### What about other countries ?

_Don't reinvent the wheel_
_Step on the shoulder of the giants_

---

#### Swiss ReGBL, since 2015

  - a federal common registry of addresses, buildings, housings
  - since 2015
  - Team of 15 for the whole country
  - huge economic and effiency savings

https://www.housing-stat.ch
![](https://codimd.s3.shivering-isles.com/demo/uploads/b48590c2-98da-4fa8-a09e-b530be7bfee7.png)


---


#### 3DBag NL

  - 3D LIDAR reconstruction.
  - Mostly focused on geometry and 3D volumes (LOD 2.1)
  - Excellent FOSS libraries !

![](https://codimd.s3.shivering-isles.com/demo/uploads/395e17f6-681b-4ec4-9616-006ffa3a6855.png)

https://3dbag.nl

---

#### Dubai Building Code (DBC) 

>The objective of the Dubai Building Code (DBC) is to unify building design across Dubai, and to create a building code that is easy to use [..]

![](https://codimd.s3.shivering-isles.com/demo/uploads/4baa6b46-313c-43a4-8cac-4260f6cec1d8.jpg =250x)



---

### What's in the box ? 

>Base de Données Nationale des Bâtiments

_metrics_ 


---

### France metropolitan & Corsica dataset 

---


![](https://codimd.s3.shivering-isles.com/demo/uploads/63b7b890-eabc-4184-8729-e96647193b4e.png =600x)

---

23 Millions of buildings groups

41 millions of street addresses

70 millions of housings

90 millions of parcels

20-30 external datasets 

400 columns / properties


---

![](https://codimd.s3.shivering-isles.com/demo/uploads/91826247-8153-44e4-bd8b-ab13507e2f05.png)


---

~ 12% of buildings with a Energy performance diagnosis

Simulated DPE on all buildings

Energy consumption (electricity / gaz) at annual / monthly step

Social housing status / ownership status


---

### How to build such a massive database?

_Go open, Go AGILE_
_Learn from free open source collaboration methods_ 

---

#### Release early - Release often

*2 versions per year*

- no relational model at start
- many list fields
- easy to understand and use as a massive dataframe
- not easy to query efficiently and use in a GIS

---


### The hardest parts 

---

##### Address matching

![](https://codimd.s3.shivering-isles.com/demo/uploads/a9297918-201d-4116-aecc-a7f5f5089cee.png =700x)

---

####  Infer what housing in what building on a parcel.

Building inference using combinatorial optimization techniques

![](https://codimd.s3.shivering-isles.com/demo/uploads/67fd7d46-3a7d-4451-b054-86e03b333cad.png)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/dd8695a8-4302-4b7d-a89a-496ded32d7a0.png)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/8a0a6351-a49a-4df2-9df8-237a1b0aeb56.png)



---


### Make it easy to re-use

Go open data as soon as possible

---

#### First export formats

GPKG

SQL postgis export (thanks OGR2OGR) - > most downloaded

CSV exports + WKT field -> almost never used

---

Provide QGIS project file with GPKG

>Download and use

![](https://codimd.s3.shivering-isles.com/demo/uploads/e858e848-fb5d-4982-bc98-b57857cdc659.png =700x)


---

#### Collect feedback and help users

- data.gouv.fr the french open data platform
- discussions
- declare reuse application
- gitlab for documentation and ticketing 

-> focus quickly on the real pain points and real added value

---

#### Iterate and improve

Next version :

- clarify building's definition:
  - building group : same parcel, same owner. Links to most external datasets
  - physical building 
  - building = main entrance + step : this should be the real object, but we must infer properties 


---

Refactor the data model to proper relational

![](https://codimd.s3.shivering-isles.com/demo/uploads/093c6270-9412-4065-af04-b2537c3167fc.png)


---

Includes all building's footprints, even no external information.

---

#### A bet, go open-data + premium services

---

#### The tragedy of the commons:

Everyone tries to recompute a building database

No one has the ressources to maintain it

Let's mutualize and push the government and ministries to build a real reference dataset

---

#### BatID project

- public startup to bootstrap the project
- identify breaks and bottlenecks
- Propose a first data model and definitions
- test it with BDNB real data
- Change the law and have it done a lot quicker than usual public projects


![geocommuns logo](https://codimd.s3.shivering-isles.com/demo/uploads/7a690b0a-9562-4166-ace1-0b17b3e55f73.png =200x)

https://github.com/entrepreneur-interet-general/batid


---

### Our commercial services 

##### Build a sustainable economic model

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---

Deliver private data to eligible users (public authorities) in a usable format

Enrich with ad-hoc and newest predictions (prices / heat sensibility etc..)

Provide service and experts

Provide efficient API's with a secured access model

---


###### Pre-populate simulation engines

District MOdeller and SIMulator (DIMOSIM) – A dynamic simulation platform based on a bottom-up approach for district and territory energetic assessment

[UrbanPrint](https://efficacity.com/quartiers-bas-carbone/nos-logiciels/urbanprint/) : Decision making tool based on Life Cycle Assessment of district making project

Typy : a library providing typologies of data for buildings. Maps and enrich BDNB informations

---

#### Enrich the database 

Predict missing Energy Performance Diagnosis (DPE) using machine Learning


![before_after_dpe2021](https://codimd.s3.shivering-isles.com/demo/uploads/cc1cfa33-2f8e-4159-bb7b-83e8b4a73615.png =600x)


_[Methodology here](https://gitlab.com/BDNB/base_nationale_batiment/-/wikis/M%C3%A9thodologie/M%C3%A9thodologie-de-pr%C3%A9diction-des-classes-de-DPE-2021)_

---


### Our infrastructure

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---

#### Data production 

3 bare metal servers 

- Postgres self hosted (7 TB / 48 CPU / 180 Go RAM/ NVme)
- JupyterLab for datascientists
- Spark for massive // computing
- REDDIS + ADDOK for massive Geocoding



---

#### Cloud infrastructure 

* SaaS infrastructure for web apps and API's
* Scaleway hosting (~~GCP~~) - RGPD compliant
* Kubernetes
* PostgreSQL managed

---

#### Backend

- PostgREST: ![postgrest logo](https://codimd.s3.shivering-isles.com/demo/uploads/efebe5de-a55b-4d71-9094-a1ce6bc95a90.png =200x)


- API REST directly from tables, views or stored procedures
- Access control using DB roles. + Row level security policies

- Gravitee : Gateway for API

- Keycloak: authentication :  jwt tokens > PG Row level policies 


---

## Future Roadmap

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

---

#### Keep on pushing hard to open new datasets

- access to the public aids for retrofitting ! High priority
- Synchronize IGN, French ministry of tax raising, INSEE to make unified datasets for housings, buildings, parcels, adresses and streets

---

#### Better documentation

- markdown in a Docsy-Hugo static website
- generate html data model and dictionnary from SQL metamodel

---

#### Go v1.0

- API stability on core model
- generic data model

---

#### try cool nerdy stuff :nerd_face:

- Geoparquet export (optimized column storage)
- FlatGeoBuff ? (cloud optimised)
- OGC API For Features

---

#### Get 3D volumes of buildings

![lidar_qgis](https://codimd.s3.shivering-isles.com/demo/uploads/179f43b1-c82a-4fa4-a401-e42da9b28805.png)

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/b433f863-fa5f-47ef-9db4-d5604ac0ff22.png)


---

- better thermal simulation
- solar potential (requires also tree detection)
- getting closer from BIM

---

### How ?

- Reconstruction library : 3D-BAG tools (NL).
- LIDAR data : IGN LIDAR
- Storage : postGIS


---

## Follow us
![](https://codimd.s3.shivering-isles.com/demo/uploads/5a2e3534-540d-4822-8ee5-6aa78409e721.png =40x)

 [@bdnb_team](https://twitter.com/BDNB_team)


![](https://codimd.s3.shivering-isles.com/demo/uploads/604bc068-1fd1-4e93-88de-ca41a1d3cb60.png =100x)


Gitlab public tracker and wiki [<https://gitlab.com/BDNB/base_nationale_batiment>](https://gitlab.com/BDNB/base_nationale_batiment)

<!-- .slide: data-background="https://codimd.s3.shivering-isles.com/demo/uploads/b5c50804-9333-45bc-a964-04f8652ccfd1.png" data-background-repeat="cover"  -->

